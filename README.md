# Automated team tracking for Support Pairing sessions

You have a [team tracking](https://gitlab.com/gitlab-com/support/support-pairing/-/blob/master/.gitlab/issue_templates/Ticket%20Pairing%20-%20Team%20Tracking.md) issue and want to reach the goal of pairing at least once with everyone Support Engineer? This project can automatically update your tracking issue once a week. That way you can spend less time copy&pasting pairing session issue links and clicking checkboxes and more time enjoying the pairing sessions!

## Why?

The goal is to foster more 1:1 connections within the Support Team. Once you're settled into your role, it's easy to stick to who you already know well. With some gamification we can help break that up a little, and thus help create an even stronger safety net for us all.

### Limitation to 1:1 pairings

While group pairings are awesome, there's a special bond that forms in the more personal context of 1:1 pairings. This project aims to increase the amount of these connections within the team, so it will only count people you have had 1:1 pairings with.

There's a [Backlog issue](https://gitlab.com/gitlab-com/support/toolbox/team-tracking/-/issues/30) for group pairings.

### Limitation to Support Engineers

Pairings with Support Managers or Support Ops are both rare and usually also serve a slightly different purpose. Limiting the scope to just Support Engineers also makes it easier to actually complete the list.

There's a [Backlog issue](https://gitlab.com/gitlab-com/support/toolbox/team-tracking/-/issues/29) to further iterate on this.

# Usage

* Add the `automate-me` label to your team tracking issue.
* That's it. 🚀

The issues are updated every Monday at 02:15 UTC via a scheduled pipeline.

# Contributing

Please do! ✨

Questions, ideas, problems? Feel free to open an issue, or reach out on Slack in the [#spt_team_tracking_project](https://gitlab.slack.com/archives/C052V4S1GDQ) channel.

## Running locally

Prerequsites: You'll need to have `pip` or `pip3` available on your `$PATH`. Using a virtual environment is recommended.

⚠️ This _will_ change the actual team tracking issues. They are easily reset, so this is fine – but be aware.

1. Clone project
1. `pip install -r requirements.txt`
1. Get a Personal Access Token with `api` scope
1. `python3 tt.py $GITLAB_TOKEN`
1. Check updated team tracking issue descriptions
