"""
Calculate and publish stats for 1:1 pairings in Support
"""

import sys
from datetime import datetime, timezone
from collections import defaultdict
import os
import glob
import yaml
import gitlab

# Get Personal Access Token from command line execution
gl = gitlab.Gitlab(private_token=sys.argv[1])

SUPPORT_PAIRING_PROJECT_ID = 14978605
PROJECT_URL = "https://gitlab.com/gitlab-com/support/toolbox/team-tracking"

def team_members_by_region(user_id):
    """Calculate list of all Support Engineers grouped by region"""
    # Expects content of https://gitlab.com/gitlab-com/support/team be present in ./team

    team = []

    for file in sorted(glob.glob('team/data/agents/**/*.yaml', recursive=True)):
        with open(file, 'r', encoding="utf-8") as yaml_file:
            team.append(yaml.safe_load(yaml_file))

    team_members = defaultdict(dict)
    team_members['AMER-C'] = []
    team_members['AMER-E'] = []
    team_members['AMER-W'] = []
    team_members['APAC'] = []
    team_members['EMEA'] = []

    for team_member in team:
        if team_member['gitlab']['id'] == user_id:
            print("⏭️ Skip the self.")
        elif 'Support Engineer' not in team_member['title']:
            print("❎ Skipping " + team_member['name'] + " due to role; our focus is Support Engineers 🍐 pairing with other Support Engineers.")
        else:
            team_members[team_member['region']].append(team_member)

    return team_members

def team_tracking_issues():
    """Retrieve all team tracking issues with the `automate-me` opt-in label"""

    project = gl.projects.get(SUPPORT_PAIRING_PROJECT_ID)
    issues = project.issues.list(state='opened', labels=['automate-me'], all=True)

    return issues

def is_oneonone_pairing(issue):
    """Check if a pairing issue has exactly two assignees"""

    return len(issue.assignees) == 2

def first_pairings_for_user(user_id):
    """Retrieve pairing data for a user"""
    project = gl.projects.get(SUPPORT_PAIRING_PROJECT_ID)
    issues = project.issues.list(assignee_id=user_id, state='closed', order_by='created_at', sort='asc', all=True)

    first_pairing_id_by_partner_id = {}

    for issue in issues:
        if is_oneonone_pairing(issue):
            for assignee in issue.assignees:
                if (assignee['id'] != user_id) and (assignee['username'] not in first_pairing_id_by_partner_id):
                    first_pairing_id_by_partner_id[assignee['username']] = issue
                    print("🍐 First pairing with " + assignee['username'] + ": " + str(issue.id) + ", " + issue.web_url)

    return first_pairing_id_by_partner_id

def create_footer():
    """Calculate meta information to be added at the bottom of the issue"""

    footer = "\n\n\n" + "<small>"
    footer += "ℹ️ Last updated on **" + datetime.today().astimezone(timezone.utc).strftime('%Y-%m-%d %H:%M:%S %Z') + "**"

    # If we're running in a pipeline, add extra debug info
    if 'CI_COMMIT_SHORT_SHA' in os.environ:
        commit_sha = os.environ['CI_COMMIT_SHORT_SHA']
        pipeline_id = os.environ['CI_PIPELINE_ID']

        footer += " with version [" + commit_sha + "](" + PROJECT_URL + "/-/commit/" + commit_sha + ")"
        footer += " of the [team-tracking](" + PROJECT_URL + ") project."
        footer += " (Pipeline [#" + pipeline_id + "](" + PROJECT_URL + "/-/pipelines/" + pipeline_id + "))"
    else:
        footer += " from a development environment of the [team-tracking](" + PROJECT_URL + ") project."

    return footer + "</small>"

def render_issue_body(user_id):
    """Create Markdown output for a users team-tracking issue"""

    cached_team_members_by_region = team_members_by_region(user_id)
    first_pairing_id_by_partner_id = first_pairings_for_user(user_id)

    # Create a dictionary mapping region name to an appropriate emoji
    regional_emoji = {
            "AMER-C": "🌎",
            "APAC": "🌏",
            "AMER-E": "🌎",
            "EMEA": "🌍",
            "AMER-W": "🌎",
        }

    issue_body = ''

    for region in cached_team_members_by_region:
        # Build list and amount of all usernames within the current region
        usernames_in_region = [x['gitlab']['username'] for x in cached_team_members_by_region[region]]
        potential_pairing_partner_amount = len(usernames_in_region)

        # Build list and amount of all usernames that are present in first_pairing_id_by_partner_id, i.e. have been paired with
        paired_in_region = [x for x in usernames_in_region if x in first_pairing_id_by_partner_id]
        actual_pairing_partner_amount = len(paired_in_region)

        # Calculate percentage for region
        regional_percentage = round(actual_pairing_partner_amount / potential_pairing_partner_amount * 100)

        issue_body = issue_body + "\n\n" + '### ' + regional_emoji[region] + " " +  region + " (" + str(actual_pairing_partner_amount) + "/" + str(potential_pairing_partner_amount) + ", " + str(regional_percentage) + "%)\n"
        for team_member in cached_team_members_by_region[region]:
            if team_member['gitlab']['username'] in first_pairing_id_by_partner_id:
                issue_body = issue_body + "\n - [x] [" + team_member['name'] + "](" + first_pairing_id_by_partner_id[team_member['gitlab']['username']].web_url + ")"
            else:
                issue_body = issue_body + "\n - [ ] " + team_member['name']

    issue_body = issue_body + create_footer()
    return issue_body

def main():
    """Update all opted-in team-tracking issues with current data"""

    # Iterate over all users that have opted in via the `automate-me` label
    for current_issue in team_tracking_issues():
        user_id = current_issue.author['id']
        print("ℹ️ Processing team-tracking issue for " + current_issue.author['name'] + "…")

        # Update issue body with new data
        current_issue.description = render_issue_body(user_id)
        current_issue.save()

if __name__ == "__main__":
    main()
